
var log4js = require('log4js');
var EventLogger = require('node-windows').EventLogger;
var log = new EventLogger('Simio');

var clients=[];
var socketClients=[];
var socketUsers=[];
var requests = [];
var responses = [];
var users = [];
var API_KEY_LIMIT_TIME = 100*6*60; //limit  for testing only
var canSend;
var cookie = require('cookie');

var mainjs = require('../Main');
var io = require('socket.io').listen(mainjs.server);

var http = require('http');


var DOWNLOAD_DIR = './downloads/';
var SETUPS_DIR = './setups/';
var fs = require('fs');

io.sockets.on('connection', function (socket) {
	if (socket.handshake.headers.cookie) {

		var cookies = cookie.parse(socket.handshake.headers.cookie);
		//var apikey_idx= cookies.split(';').indexOf('api_key');
		var api_key = cookies.api_key//cookies.split(';')[apikey_idx].split('=')[1];
		//console.log('cookie parser - > ' +  cookie.parse(socket.handshake.headers.cookie));
		//console.log('api_key -> ' + api_key);
		//console.log('key -> ' + cookies.split(';')[0].split('=')[1]);
		socketClients['' + api_key] = socket;
		//console.log('save the socket id -' + socket.id + '- with api key ' + api_key);
		socketUsers[socket.id] = api_key;
	}
});

io.sockets.on('disconnect', function (socket) {
	//broadcast(socket.name + " left the chat.\n");
	//console.log('socked -' + socket.id + '- disconnected');

	var api_key = socketUsers[socket.id];
	if(api_key!== null && api_key !== undefined)
	{
		//console.log('found socket match to send logout');
		if(clients[api_key] !== null && clients[api_key] !== undefined)
		{
			var json = JSON.parse(clients[api_key]);
			if(json !==null && json!==undefined)
			{
				//console.log('send logout to user -> ' + json.UserName);
				//sendMessageToServer(buildMsg('','','logout','Login',JSON.stringify(json)));
				//delete clients[api_key];
			}
		}
	}
});

io.sockets.on('error', function () {
	console.log('Socket.io Error');
});

//io.sockets.on('yuval', function (data) {
//	console.log('Got hide message' + data);
//});

exports.loadSocket = function(socketServer) {

    var data = "";

    // Load the TCP Library
    net = require('net');
    JsonServer = require('json-socket');

    var port = 4001;
    var server = net.createServer();
    server.listen(port);
    server.on('connection', function (soc) { //This is a standard net.Socket
        soc = new JsonServer(soc); //Now we've decorated the net.Socket to be a JsonSocket
        soc.on('message', function (message) {

//			console.log('Got from server:' + JSON.stringify(message));
            try {
                var json = message;

                //iterate all connected sockets
//				for (var key in socketClients) {

                //console.log('socket -> ' + key);
                //iterate the allowed users returned from the server
                //for(var i in data.Users)
//					for (var i = 0; json.Users.length > i; i++) {
                //console.log('user -> ' + item;
                //get the user id
//						var item = json.Users[i];
                //console.log('allowed user id -> ' + item);
//						if (clients[key] !== undefined) {
                //console.log('found socket to parse');
//							var clientJson = JSON.parse(clients[key]);
                //console.log('after parse socket json -> ' + JSON.stringify(clientJson));
                //checks if the allowed user is equal to the current socket user
//							if (item == clientJson.UserID) {
                //console.log('found socket match to allowed user -> ' + item);
                var data2Push = '';
                //console.log('Event: '+ json.EventName);
                //push to client and break the iteration
                switch (json.EventName) {
                    case "deviceConnected":
                    case "deviceRemoved":
                    case "deviceChanged":
                        if (json.Device !== null && json.Device !== undefined)
                            data2Push = json.Device;//JSON.stringify( json.Device);
                        else
                            data2Push = message;//JSON.parse(message.toString());
                        break;
                    case "alarmNotification":
                        data2Push = json.Alarms;
                        break;
                   default:
                        data2Push = message;//JSON.parse(message.toString());
                        break;
                }
//								console.log('push to client -> ' + JSON.stringify(data2Push));
                //socketClients[key].emit(json.EventName, data.toString());
                //socketClients[key].emit(json.EventName, data2Push);
                io.sockets.emit(json.EventName, data2Push);
                data = "";
//								i = json.Users.length + 1;
//							}
//						}
//					}
//				}
                //********************************************************************
                //send ACK
                soc.sendMessage('ACK');
            }
            catch (e) {
                console.log('Data not complete from Socket')
            }
        });

        soc.on('error', function (err) {
            console.log("Caught node socket error: " + err.stack);
        });

    });

    // Put a friendly message on the terminal of the server.
    console.log("Nodejs tcp server running at port 4001\n");
};

/*var netClient = require('net'),
	JsonSocket = require('json-socket');
var HOST = '127.0.0.1';
//var HOST = '10.10.10.75';
//var HOST = 'simio.cloudapp.net';
var PORT = 4000;
var client = new JsonSocket(new netClient.Socket());
client.connect(PORT,HOST);

client.on('connect', function() {
	console.log('Connection to Server is ON');
});

client.on('close', function() {
	console.log('Server Closed Connection');

	setTimeout(function() {
		client.connect(PORT, HOST, function(){
			console.log('CONNECTED TO: ' + HOST + ':' + PORT);
		});
	},5000);
	console.log('Timeout for 2 seconds before trying port:' + PORT + ' again');
});

client.on('error', function(e) {
	if(e.code == 'ECONNREFUSED' || e.code == 'ENCONNRESET') {
		console.log('Got Server' + e.code);

		setTimeout(function() {
			client.connect(PORT, HOST, function(){
				console.log('CONNECTED TO: ' + HOST + ':' + PORT);
			});
		},5000);

		console.log('Timeout for 2 seconds before trying port:' + PORT + ' again');
	}
});
*/
function sendMessageToServer(msg)
{
    //console.log('send to server - ' + msg);

    APIPost(msg);

/*
	client.sendMessage(msg);

	client.on('message', function(data) {
		try
		{
			//in login - get from the response the api_key and save it for the session id
			if(data !== null && data !== undefined) {
				var obj = data;
                var response = responses[obj.RequestID];
				//checks if the object contains API_KEY
				if (obj.API_KEY !== null && obj.API_KEY !== undefined) {
					var unixtime = Math.round((new Date()).getTime() / 1000);
					//if the action is validate and the entity is login
					//take the user id and save it in a JSON format: {UserID:1234,UnixTIme:123322333}
					if (obj.Command == 'validate' && obj.Entity == 'Login') {

						if (obj.Result == "Error") {
							//var response = responses[obj.RequestID];
							if (response != undefined) {
								response.statusCode = 401;
							}
						}
						else {
							//checks if the user already exists in the users object
							if (users[obj.UserID] !== null && users[obj.UserID] !== undefined) {
								//console.log('user exists - ' + obj.UserID + ' getting the request ... ');
								//get the request from the existing list
								var request = requests[obj.RequestID];
								//if found something
								if (request !== null && request !== undefined) {
									//console.log('found request, checks if ip is the same : ' + request.connection.remoteAddress + ' <-> ' + users[obj.UserID]);
									//checks if it is from the same IP
									if (request.connection.remoteAddress !== users[obj.UserID]) {
										//console.log('deleting the old request object');
									}
									//save the new one
									users[obj.UserID] = request.connection.remoteAddress;
								}
							}
							//console.log('save userid- > ' + obj.UserID);
							var userid = obj.UserID;
							clients[obj.API_KEY] = JSON.stringify({
								'UserID': userid,
								'unixtime': unixtime,
								'UserName': obj.UserName
							});
							//var response = responses[obj.RequestID];
							if (response != undefined) {
								response.statusCode = 200;
								var d = new Date();
								d.setTime(d.getTime() + 24 * 60 * 60 * 1000); // in milliseconds

								response.cookie('api_key', obj.API_KEY, {maxAge: d.toGMTString(), httpOnly: true});
								response.cookie('user', JSON.stringify({
									'username': obj.UserName,
									'role': obj.RoleID
								}), {maxAge: d.toGMTString(), httpOnly: false});
							}
						}
					}
					else {
						//console.log('update unixtime');
						var json = JSON.parse(clients[obj.API_KEY]);
						json.unixtime = unixtime;
						//console.log('userid updated -> ' + json.UserID);
						clients[obj.API_KEY] = JSON.stringify(json);
					}
				}
				//get the response object
				//var response = responses[obj.RequestID];
				if (response != undefined) {
					//if contains error then return status code 500
					if (obj.Result.toUpperCase().indexOf('ERROR') >= 0)
						response.statusCode = 500;
					//delete the request
					delete responses[obj.RequestID];
					//send back the response
					response.json(data);
				}
				//else
				//{
				//	//console.log(obj.RequestID);
				//	//console.log(obj.Entity);
				//}

			}
			//client.write('ACK');
		}
		catch(e)
		{
            console.log('Fail in parsing message from server... ');
			console.log(e);
		}
	});
*/
}

function sendError(errCode, errString, response){
    response.writeHead(errCode, {"Content-Type": "application/json"});
    response.write(errString + "\n");
    response.end();
}

function getFile(response, localpath){
    fs.readFile(localpath, "binary",
        function(err, file){ sendFile(err, file, response);});
}

function sendFile(err, file, response){
    if(err) return sendError(500, err, response);
    //response.writeHead(200,{
    //    "Content-Disposition": "attachment; filename='certificate.pdf'",
    //    "Content-Type": "application/pdf"
    //});
    response.write(file, "binary");
    response.end();
}

function APIPost(msg){
    var jsonData = JSON.parse(msg);
    var requestId = jsonData.RequestID;
    var method  = jsonData.Command=='get'?'GET':'POST';
    //console.log('method - > ' + method);
    // An object of options to indicate where to post to
    var post_options = {
        host: 'localhost',
        port: '4000',
        path: '',
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': msg.length
        }
    };

    try {
        //var file;
        //var filePath ='';
        //if (jsonData.Command === 'certificate') {
        //    var file_name = 'certificate-' + jsonData.Data.SerialNumber + '.pdf';
        //    filePath=DOWNLOAD_DIR + file_name;
        //    file = fs.createWriteStream(filePath);
        //}


        // Set up the request
        var post_req = http.request(post_options, function (res) {

            res.setEncoding('utf8');
            var output = '';
            res.on('data', function (data) {

                //if (jsonData.Command === 'certificate') {
                //    console.log('write the pdf to a file...');
                //    file.write(data);
                //}
                //else {
                    output += data;
                //}
            });

            res.on('end', function () {

                try {

                    //if (jsonData.Command === 'certificate') {
                    //    console.log('close the file and return it back to client as pdf');
                    //    //    file.end();
                    //    //
                    //    //    file.on('finish', function () {
                    //    var responseFile = responses[requestId];
                    //    if (responseFile !== undefined) {
                    //        responseFile.statusCode = 200;
                    //        delete responses[requestId];
                    //        //the link of the file will be in the json returned
                    //        getFile(responseFile, DOWNLOAD_DIR + 'report-913742.pdf');
                    //    }
                    //    //    });
                    //    //
                    //
                    //}
                    //else {

                        //in login - get from the response the api_key and save it for the session id
                        if (output !== null && output !== undefined) {
                            //var jsonMsg = output.split('#')[1];
                            var obj = JSON.parse(output);

                            var response = responses[obj.RequestID];
                            //checks if the object contains API_KEY
                            if (obj.API_KEY !== null && obj.API_KEY !== undefined) {
                                var unixtime = Math.round((new Date()).getTime() / 1000);
                                //if the action is validate and the entity is login
                                //take the user id and save it in a JSON format: {UserID:1234,UnixTIme:123322333}
                                if (obj.Command == 'validate' && obj.Entity == 'login') {

                                    if (obj.Result == "Error") {
                                        //var response = responses[obj.RequestID];
                                        if (response != undefined) {
                                            response.statusCode = 401;
                                        }
                                    }
                                    else {
                                        //checks if the user already exists in the users object
                                        if (users[obj.UserID] !== null && users[obj.UserID] !== undefined) {
                                            //console.log('user exists - ' + obj.UserID + ' getting the request ... ');
                                            //get the request from the existing list
                                            var request = requests[obj.RequestID];
                                            //if found something
                                            if (request !== null && request !== undefined) {
                                                //console.log('found request, checks if ip is the same : ' + request.connection.remoteAddress + ' <-> ' + users[obj.UserID]);
                                                //checks if it is from the same IP
                                                if (request.connection.remoteAddress !== users[obj.UserID]) {
                                                    //console.log('deleting the old request object');
                                                }
                                                //save the new one
                                                users[obj.UserID] = request.connection.remoteAddress;
                                            }
                                        }
                                        //console.log('save userid- > ' + obj.UserID);
                                        var userid = obj.UserID;
                                        clients[obj.API_KEY] = JSON.stringify({
                                            'UserID': userid,
                                            'unixtime': unixtime,
                                            'UserName': obj.UserName
                                        });
                                        //var response = responses[obj.RequestID];
                                        if (response != undefined) {
                                            response.statusCode = 200;
                                            var d = new Date();
                                            d.setTime(d.getTime() + 24 * 60 * 60 * 1000); // in milliseconds

                                            response.cookie('api_key', obj.API_KEY, {
                                                maxAge: d.toGMTString(),
                                                httpOnly: true
                                            });
                                            response.cookie('user', JSON.stringify({
                                                'username': obj.UserName,
                                                'role': obj.RoleID
                                            }), {maxAge: d.toGMTString(), httpOnly: false});
                                        }
                                    }
                                }
                                else {
                                    //console.log('update unixtime');
                                    var json = JSON.parse(clients[obj.API_KEY]);
                                    json.unixtime = unixtime;
                                    //console.log('userid updated -> ' + json.UserID);
                                    clients[obj.API_KEY] = JSON.stringify(json);
                                }
                            }
                            //get the response object
                            if (response != undefined) {
                                response.statusCode = 200;
                                //if contains error then return status code 500
                                if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                                    console.log('message returns from server contains ERROR');
                                    response.statusCode = 500;
                                }
                                else if(obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                                    response.statusCode = 422;
                                }
                                //delete the request
                                delete responses[obj.RequestID];
                                //send back the response
                                response.json(JSON.parse(output));
                            }
                        }
                    //}
                }
                catch (e) {
                    console.log('Fail in parsing message from server... ');
                    console.log(e);
                    var responseError = responses[requestId];
                    if (responseError != undefined) {
                        responseError.statusCode = 500;
                        delete responses[requestId];
                        responseError.json(output);
                    }
                }

            });
        });

        // post the request
        post_req.write(msg);

        post_req.on('error', function (err) {
            console.log(err);
            var responseError = responses[requestId];
            if (responseError != undefined) {
                responseError.statusCode = 500;
                delete responses[requestId];
                responseError.json(msg);
            }
        });

        post_req.end();

    }
    catch(ex) {
        console.log('Fail while trying to communicate with the Server API');
        console.log(ex);
    }
}

exports.calibrationActions = function (req,res){

    var requestid = req.id;

    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

       /* if(method!==undefined)
        {
            var json =JSON.stringify(req.body);
            if(method==='certificate') {
                var headers = {
                    'Content-Type': 'application/json',
                    'Content-Length': json.length
                };

                var options = {
                    host: 'localhost',
                    port: 4000,
                    path: '',
                    method: 'POST',
                    headers: headers
                };
                var file_name = 'certificate-' + json.serialNumber + '.pdf';
                var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

                http.get(options, function (response) {
                    response.on('data', function (data) {
                        console.log('[OLD]write the pdf to a file...');
                        file.write(data);
                    }).on('end', function () {
                        console.log('[OLD]close the file and return it back to client as pdf');
                        file.end();
                        console.log('first head....');
                        res.setHeader('Content-Length', file.length);
                        console.log('second head....');
                        res.setHeader('Content-type', 'application/pdf');
                        console.log('write the file');
                        res.write(file, 'binary');
                        console.log('response back...');
                        res.end();
                    });
                });

            }
            else {*/
                console.log('calibrate method -> ' + method);
                //validate the session id (api key)
                if (validateSession(requestid, req, res))
                    sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'Calibration', JSON.stringify(req.body)));
          /* }
        }*/
    }
};

//change to build the message in the new format
function buildMsg(apikey,requestid,command,entity,data){
	//change the format to {RequestID:,UserID:,Command:,Entity:,Data:}
	//on each action, send the user id to validate if is allowed to do this action
	//get the user id from the clients object
	//console.log('api key -> ' + apikey);
	var userid = 0;
	if(apikey.length>0 && clients[apikey]!=='null' && clients[apikey] !=='undefined')
	{
		//console.log('try to get the json object');
		var objJson = JSON.parse(clients[apikey]);
		//console.log(objJson);
		//console.log('the userid found -> ' + objJson.UserID);
		userid = objJson.UserID;
    }

	var json ='{"RequestID":"'  + requestid + '","UserID":"' + userid + '","Command":"' + command + '","Entity":"' + entity + '","Data":' + data + '}';
	return (json);
}

function validateSession(requestid,req,res){

	var unixtime = Math.round((new Date()).getTime() / 1000);

	var apikey= req.cookies.api_key;

	if(clients[apikey] !== null && clients[apikey] !== undefined)
	{
		var json =  (JSON.parse(clients[apikey]));
		//console.log('after parse the clients object');
		//console.log(json);
		var savedUnixtime = json.unixtime;

		//console.log('1-> ' + clients[apikey]);
		//console.log('unixtime - > ' + unixtime);

		if((unixtime-clients[apikey])>API_KEY_LIMIT_TIME)
		{
			//console.log('session expired...');
			delete clients[apikey];
			res.statusCode =401;
			res.statusText="session expired! Please re-login";
			res.send({data:{Error:'session expired! Please re-login'}});
		}
		else
		{
			//console.log('session is ok :-) ');
			//renew the session time with the current time
			//clients[req.cookies.api_key] = unixtime;
			var json = JSON.parse(clients[apikey]) ;
			json.unixtime =  unixtime;
			clients[apikey]  = JSON.stringify(json);
			//save this request id
			responses[requestid] = res;
			requests[requestid] = req;//req.connection.remoteAddress
			users[json.UserID]=req.connection.remoteAddress;
			res.statusCode =200;
			res.statusText="";
			return 1;
		}
	}
	else
	{
		//console.log('no api key found');
		res.statusCode =401;
		res.statusText="session expired! Please re-login";
		res.send({data:{Error:'session expired! Please re-login'}});
	}

	return 0;
}

function extractMethod(req){
	var method = req.url.slice(req.url.indexOf('/api/') + 5);
	//console.log(method);
	if(method.indexOf('/')>0){
		method = method.slice(0,method.indexOf('/'));
	}
	else if (method.indexOf('?')>0){
		method=method.slice(0,method.indexOf('?'));
	}
	else
		method = method.slice(0);
	return method.toLowerCase();
}

function getJsonParams(req){
	var json='{';
	var i=0;
	//extract the params names and their values
	for(var param in req.route.params)
	{
		json += param + ':' + req.params[param].split('=')[1];
		i++;
	}
	json+='}';

	return json;
}

exports.session = function(req,res){
	var requestid = req.id;

	if(validateSession(requestid,req,res)==0)
		{
			res.statusCode =401;
			res.send('session expired! Please re-login');
		}
	else
		{
			res.statusCode =200;

			if(clients[req.cookies.api_key]!== undefined && clients[req.cookies.api_key].UserID!== undefined)
			{
				//get the user details according to user id that extract from the api_key
				var json = '{UserID:' +  clients[req.cookies.api_key].UserID + '}';
				sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'get','Login',json));
			}
			else
			{
				res.statusCode =401;
				res.send('session expired! Please re-login');
			}
		}
};

exports.Restore = function(req,res){
	var requestid = req.id;
	if(validateSession(requestid,req,res))
	{
		var json = getJsonParams(req);
		//logger.debug('params - ' + json);

		sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'restore','dbmaintain',json));
	}
};

exports.Backup = function(req,res){
	var requestid = req.id;
	if(validateSession(requestid,req,res))
	{
		var json = getJsonParams(req);
		//logger.debug('params - ' + json);

		sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'backup','dbmaintain',json));
	}
};

exports.getQuery = function(req,res){
	var requestid = req.id;

	var method = extractMethod(req);


	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		if(req.query.CategoryID !==null && req.query.CategoryID!== undefined)
		{
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'category','devices','{CategoryID:' + req.query.CategoryID + '}'));
		}
		else if(req.query.SiteID !==null && req.query.SiteID!== undefined
				&&
				req.query.UserID !==null && req.query.UserID!== undefined )
		{
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'site','devices','{SiteID:' + req.query.SiteID + ',UserID:' + req.query.UserID + '}'));
		}
		else if(req.query.DeviceID !==null && req.query.DeviceID!== undefined)
		{
			//this is for single device
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'device','devices','{DeviceID:' + req.query.DeviceID + '}'));
		}

	}
};

exports.device = function(req,res){
	var requestid = req.id;
	var method = extractMethod(req);
	var json = getJsonParams(req);

	//validate the session id (api key)
	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'device',json));
	}

};

exports.deviceActions = function(req,res){
	var requestid = req.id;

	if(req.query!== null && req.query !== undefined){

		var method = req.query.Action;

		if(method!==undefined)
		{
			console.log('method -> ' + method);
			//validate the session id (api key)
			if(validateSession(requestid,req,res))
			{
				//call to core to get the response
				sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'device',JSON.stringify(req.body)));
			}
		}
	}
};

exports.sitesActions = function(req,res){
	var requestid = req.id;

	if(req.query!== null && req.query !== undefined){

		var method = req.query.Action;

		if(method!==undefined)
		{
			console.log('method -> ' + method);
			//validate the session id (api key)
			if(validateSession(requestid,req,res))
			{
				//call to core to get the response
				sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'sites',JSON.stringify(req.body)));
			}
		}
	}
};

exports.alarmActions = function(req,res){
	var requestid = req.id;

	//var method = extractMethod(req);
	if(req.query!== null && req.query !== undefined){


		var method = req.query.Action;

		console.log('method -> ' + method)

		//validate the session id (api key)
		if(validateSession(requestid,req,res))
		{
			//call to core to get the response
			sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'alarms',JSON.stringify(req.body)));
		}
	}
};

exports.reportsActions = function (req,res){

    var requestid = req.id;

    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

        console.log('method -> ' + method);

        //validate the session id (api key)
        if(validateSession(requestid,req,res))
            //call to core to get the response
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'reports',JSON.stringify(req.body)));
    }

};

exports.analyticsActions = function(req,res){
	var requestid = req.id;

	//var method = extractMethod(req);
	if(req.query!== null && req.query !== undefined){


		var method = req.query.Action;

		console.log('method -> ' + method);

		//validate the session id (api key)
		if(validateSession(requestid,req,res))
			//call to core to get the response
			sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'analytics',JSON.stringify(req.body)));
	}
};

exports.userActions = function(req,res){
	var requestid = req.id;
	//var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined) {
        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if (validateSession(requestid, req, res))
        //call to core to get the response
            sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'user', JSON.stringify(req.body)));
    }
};

exports.passwordRecovery = function(req,res){
	var requestid = req.id;

	var method = extractMethod(req);

	responses[requestid] = res;
	requests[requestid] = req;

	//call to core to get the response
	sendMessageToServer(buildMsg("",requestid,method.toString().toLowerCase(),'user',JSON.stringify(req.body)));

};

exports.getParams = function(req,res){
	var requestid = req.id;
	var method = extractMethod(req);
	var command = 'get';

	var data ='';
	if(req.query!== null && req.query !== undefined){
		data = req.query;
	}

	if (typeof data.Action != 'undefined')
		command = data.Action.toLowerCase();

	data = JSON.stringify(req.query);
	//validate the session id (api key)
	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,command,method,data));
	}

};

var urlObject = require('url');

exports.getFile = function(req,res){
    if(req.query!== null && req.query !== undefined){
        var action = req.query.Action;

        res.writeHead(200,{
            "Content-Disposition": "attachment; filename='" + urlObject.parse(action).pathname.split('/').pop() + "'",
            "Content-Type": "application/pdf"
        });

        var options = {
            host: urlObject.parse(action).host,
            port: 80,
            path: urlObject.parse(action).pathname
        };

        var file_name = urlObject.parse(action).pathname.split('/').pop();
        var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

        http.get(options, function(response) {
            response.on('data', function(data) {
                file.write(data);
            }).on('end', function() {
                file.end();
                getFile(res,DOWNLOAD_DIR + file_name);
            });
        });

    }

};

exports.get = function(req,res){
	var requestid = req.id;
	var method = extractMethod(req);

	//validate the session id (api key)
	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'get', method,''));
	}

};

exports.del = function(req,res){
	var requestid = req.id;
	//extract the method from the api call
	var method = extractMethod(req);
	var json = getJsonParams(req);

	//validate the session id (api key)
	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		//zmqRequest.send(buildMsg(req.cookies.api_key,requestid,'delete', method,json));
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'delete', method,json));
	}
};

exports.update = function(req,res) {

	var requestid = req.id;
	var method = extractMethod(req);

	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		//zmqRequest.send(buildMsg(req.cookies.api_key,requestid,'update', method,JSON.stringify(req.body)));
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'update', method,JSON.stringify(req.body)));
	}
};

exports.setup = function(req,res) {

	var requestid = req.id;
	var method = extractMethod(req);

	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		//zmqRequest.send(buildMsg(req.cookies.api_key,requestid,'setup', method,JSON.stringify(req.body)));
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'setup', method,JSON.stringify(req.body)));
	}
};

exports.create = function(req,res) {

	var requestid = req.id;
	var method = extractMethod(req);

	if(validateSession(requestid,req,res))
	{
		//call to core to get the response
		//zmqRequest.send(buildMsg(req.cookies.api_key,requestid,'create', method,JSON.stringify(req.body)));
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'create', method,JSON.stringify(req.body)));
	}
};

exports.SMSDelivery = function(req,res) {

	sendMessageToServer(buildMsg('',req.id,'smsdelivery', 'sms',JSON.stringify(req.body)));

};

exports.login = function(req, res) {

	var requestid = req.id;

    //save the response object with the request-id key
	responses[requestid] = res;
	requests[requestid] = req;
	//send to the core to get the response
	//zmqRequest.send(buildMsg('',requestid,'validate','Login',JSON.stringify(req.body)));
    var json = buildMsg('',requestid,'validate','login',JSON.stringify(req.body));
    sendMessageToServer(json);


};

exports.logout = function (req, res) {
	res.statusCode = 200;

	if (req.cookies.api_key) {
		delete clients[req.cookies.api_key];
        res.clearCookie('api_key');
		res.json({result:{data:'logged out'}});
	} else {
		res
			.status(500)
			.end({
				error: 'No api key specified'
			});
	}
};