var http = require('http');
var fs=require('fs');
var express = require('express'),
    path = require('path');
	
var querystring = require('querystring');
var url = require('url');
var socketClients=new Array();

// MAGIC HAPPENS HERE!
var opts = {

  pfx: fs.readFileSync('simio2015.pfx'),
  
  // This is where the magic happens in Node.  All previous
  // steps simply setup SSL (except the CA).  By requesting
  // the client provide a certificate, we are essentially
  // authenticating the user.
  requestCert: false,
   
  // If specified as "true", no unauthenticated traffic
  // will make it to the route specified.
  rejectUnauthorized: true,
  
  passphrase: "Fourtec2013"
};

//Redirecting and http and fourtec-simio.com to https://www.fourtec-simio.com
//There is also forwarding in goddady Domain setup.
http.createServer(function(req, res) {
    res.writeHead(301, {
        Location: "https://www." + req.headers["host"].replace("www.", "") + req.url
    });
    res.end();
}).listen(8000);

var https = require('https');
var app = express();

app.configure(function () {
    app.set('port', process.env.PORT || 4433);
    app.use(express.logger('dev'));  /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser()),
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true })),
    app.use(express.static(path.join(__dirname, 'public_dir')));
	app.use(express.cookieParser('Fourtec2013!'));
	app.use(require('connect-requestid'));
	
	app.use(express.cookieSession(
    {
        secret: process.env.COOKIE_SECRET || "Fourtec2013!"
    }));
	
	app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
    });
	
});

var api = require('./routes/api');

app.get('/api/LanRcvNetworkTree', api.getParams);
app.get('/api/LastSampleAllDevices', api.getParams);
app.get('/api/AlarmOnSites', api.getParams);
app.get('/api/GPSDevices', api.getParams);
app.get('/api/DeviceHistoryLogs', api.getParams);
app.get('/api/DeviceGraphData', api.getParams);
app.get('/api/DeviceSetupTemplate', api.getParams);
app.get('/api/DevicesLastSample', api.getParams);
app.get('/api/DeviceAlarms', api.getParams);
app.get('/api/CustomSensors', api.getParams);
app.get('/api/MeasurementUnits', api.getParams);
app.get('/api/AlarmTypes', api.getParams);
app.get('/api/ReportPeriodTypes', api.getParams);
app.get('/api/ReportFormatTypes', api.getParams);
app.get('/api/SensorTypes', api.getParams);
app.get('/api/Devices', api.getParams);
app.get('/api/GroupPermissions', api.getParams);
app.get('/api/SecuredQuestions', api.getParams);
app.get('/api/Sites', api.getParams);
app.get('/api/Categories', api.getParams);
app.get('/api/Languages', api.getParams);
app.get('/api/Users', api.getParams);
app.get('/api/Contacts', api.getParams);
app.get('/api/Me',api.session);
app.get('/api/DownloadData',api.device);
app.get('/api/Status',api.device);
app.get('/api/Sensors',api.device);
//app.get('/api/Alarms',api.getParams);
app.get('/api/Alarms', function(req, res){
    res.send({'quantity':5});
});
app.get('/api/Logout', api.logout);

//post methods with json in the body as parameters
app.post('/api/Login', api.login);
app.post('/api/Customers', api.create); 
app.post('/api/Admin', api.create);
app.post('/api/Users', api.create);
app.post('/api/CustomSensor', api.create);
app.post('/api/alarmSetup', api.create);
app.post('/api/lockNetwork', api.create);
app.post('/api/setupLanRcvCredentials', api.create);
app.post('/api/resetSensorCalibration', api.create);
app.post('/api/SensorCalibrationReminder', api.create);
app.post('/api/markDeviceTimeStamp', api.create);
app.post('/api/ProfileReport', api.create);
app.post('/api/setupSMS', api.create);
app.post('/api/Contacts', api.create);
app.post('/api/CalibrationCertificate', api.create);
app.post('/api/DeviceSetupTemplate', api.create);
app.post('/api/SMS',api.SMSDelivery);
app.post('/api/Devices/:id', api.deviceActions);
app.post('/api/Alarms/:id', api.alarmActions);
app.post('/api/Sites/:id', api.sitesActions);
app.post('/api/Sites', api.sitesActions);

app.post('/api/Analytics', api.analyticsActions);


//put (update) methods (like post methods)
app.put('/api/Users', api.update);
app.put('/api/User', api.setup);
app.put('/api/CustomSensor', api.update);
app.put('/api/Firmware', api.update);
app.put('/api/DeviceGPSCoordinates', api.update);
app.put('/api/SensorCalibration', api.update);
app.put('/api/DeviceAlarm', api.update);
app.put('/api/ProfileReport', api.update);
app.put('/api/Contacts', api.update);
app.put('/api/ResetCalibration',api.deviceActions);
app.put('/api/UpdateFirmware',api.deviceActions);
app.put('/api/Calibration',api.deviceActions);
app.put('/api/Backup',api.Backup);
app.put('/api/Restore',api.Restore);
app.put('/api/PasswordRecovery',api.passwordRecovery);
app.put('/api/SecureAnswerCheck',api.passwordRecovery);
app.put('/api/ResetPassword',api.passwordRecovery);
app.put('/api/SetupTemplate', api.deviceActions);

//delete methods (use params)
app.delete('/api/Users/:UserID', api.del);
app.delete('/api/ProfileReport/:id', api.del);
app.delete('/api/Contacts/:id', api.del);
app.delete('/api/Sites/:id', api.sitesActions);

var server = https.createServer(opts, app);
api.loadSocket(server);

server.listen(4433);
var cookie = require('cookie');


function originIsAllowed(origin) {
  // put logic here to detect whether the specified origin is allowed.
  return true;
}

