var http = require('http');
var fs=require('fs');
var express = require('express');
var path = require('path');

var https = require('https');
//var uploads = require('./routes/uploads');

var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var multer = require('multer');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');

var app = express();

var querystring = require('querystring');
var url = require('url');
var socketClients=[];
var clients=[];
var socketUsers=[];
var requests = [];
var responses = [];
var users = [];
var API_KEY_LIMIT_TIME = 5*60; //limit  for testing only
var cookie = require('cookie');
var urlObject = require('url');
//var DOWNLOAD_DIR = './downloads/';
var DOWNLOAD_DIR = __dirname + '/downloads/';
var SERVER_HOST = 'localhost';
var SERVER_PORT=4000;

//var api = require('./routes/api');

var opts = {

  pfx: fs.readFileSync('simio2015.pfx'),
  
  // This is where the magic happens in Node.  All previous
  // steps simply setup SSL (except the CA).  By requesting
  // the client provide a certificate, we are essentially
  // authenticating the user.
  requestCert: false,
   
  // If specified as "true", no unauthenticated traffic
  // will make it to the route specified.
  rejectUnauthorized: true,
  
  passphrase: "Fourtec2015"
};

//Redirecting and http and fourtec-simio.com to https://www.fourtec-simio.com
//There is also forwarding in goddady Domain setup.
http.createServer(function(req, res) {
	try{
		res.writeHead(301, {
        Location: "https://www." + req.headers["host"].replace("www.", "") + req.url
		});
		res.end();	
	}
	catch(e){
		console.log('Redirect from:' + req.headers["host"] + req.url);
	}
     
}).listen(8000);

// all environments
app.set('port', process.env.PORT || 4433);
//app.use(favicon(__dirname + '/public_dir/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({
        resave: false,
        saveUninitialized: false,
        secret: 'Fourtec2013!',
        cookie:{
            path: '/',
            httpOnly: false,
            secure: true,
            maxAge: 60
        }
    }
    ));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({dest:'./uploads/'}).single('file'));
app.use(express.static(path.join(__dirname, 'public_dir')));
app.use(cookieParser('Fourtec2013!'));
app.use(require('connect-requestid'));

//app.use(cookieSession(
//    {
//        secret: process.env.COOKIE_SECRET || "Fourtec2013!"
//    }));
app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
    });

// error handling middleware should be loaded after the loading the routes
if ('development' == app.get('env')) {
    app.use(errorHandler());
}

var server = https.createServer(opts,app);


var io = require('socket.io').listen(server);

// Load the TCP Library
var net = require('net');
var JsonServer = require('json-socket');

loadSocket();

io.on('connection', function (socket) {
    if (socket.handshake.headers.cookie) {
        console.log('Client connected.');
        var cookies = cookie.parse(socket.handshake.headers.cookie);
        //var apikey_idx= cookies.split(';').indexOf('api_key');
        var api_key = cookies.api_key;//cookies.split(';')[apikey_idx].split('=')[1];
        //console.log('cookie parser - > ' +  cookie.parse(socket.handshake.headers.cookie));
        //console.log('api_key -> ' + api_key);
        //console.log('key -> ' + cookies.split(';')[0].split('=')[1]);
        socketClients['' + api_key] = socket;
        //console.log('save the socket id -' + socket.id + '- with api key ' + api_key);
        socketUsers[socket.id] = api_key;
    }

    socket.on('disconnect', function() {
        console.log('Client disconnected.');
        var api_key = socketUsers[socket.id];
        if(api_key!== null && api_key !== undefined)
        {
            //console.log('found socket match to send logout');
            if(clients[api_key] !== null && clients[api_key] !== undefined)
            {
                var json = JSON.parse(clients[api_key]);
                if(json !==null && json!==undefined)
                {
                    //socketClients[api_key]=null;
                    delete socketClients[api_key];
                }
            }
        }
    });
});

function loadSocket()  {

    var data = "";
    var port = 4001;
    var server = net.createServer();
    server.listen(port);

    server.on('connection', function (soc) { //This is a standard net.Socket
        soc = new JsonServer(soc); //Now we've decorated the net.Socket to be a JsonSocket
        soc.on('message', function (message) {

            try {
                var json = message;
                var data2Push = '';
                var serialNumber=0;
                //console.log('Event: '+ json.EventName);
                //push to client and break the iteration
                switch (json.EventName) {
                    case "deviceConnected":
                    case "deviceRemoved":
                    case "deviceChanged":
                        if (json.Device !== null && json.Device !== undefined) {
                            data2Push = json.Device;//JSON.stringify( json.Device);
                            serialNumber = json.Device.SerialNumber;
                        }
                        else {
                            data2Push = message;//JSON.parse(message.toString());
                            serialNumber = message.SerialNumber;
                        }
                        break;
                    case "alarmNotification":
                        data2Push = json.Alarms;
                        if(json.Alarms)
                            serialNumber = json.Alarms.SerialNumber;
                        break;
                    case "downloadProgress":
                        data2Push = message;
                        serialNumber = json.SerialNumber;
                        break;
                    case "batchAdded":
                        data2Push = message;
                        serialNumber = json.SerialNumber;
                        break;
                    case "onlineStatus":
                        data2Push = message;
                        serialNumber = json.SerialNumber;
                        break;
                    default:
                        data2Push = message;//JSON.parse(message.toString());
                        break;
                }
                //go over the socket clients currently connected
                if(serialNumber>0) {
                    for (var apiKey in socketClients) {
                        if (apiKey !== undefined) {
                            if (clients.hasOwnProperty(apiKey)) {
                                var client = JSON.parse(clients[apiKey]);
                                //checks if the specific serial number exists on the clients iterated
                                if (!(serialNumber in client.AllowedDevices)) {
                                    //then push to this socket
                                    socketClients[apiKey].emit(json.EventName, data2Push);
                                }
                            }
                        }
                    }
                }
                else {
                     io.sockets.emit(json.EventName, data2Push);
                }

                data = "";
                //send ACK
                //soc.sendMessage('ACK');
            }
            catch (e) {
                console.log('Data not complete from Socket')
            }
        });

        soc.on('error', function (err) {
            console.log("Caught node socket error: " + err.stack);
        });
        soc.on('disconnect', function() {
            console.log('Got disconnect!');

            // var i = allClients.indexOf(socket);
            //delete allClients[i];
        });
    });

    // Put a friendly message on the terminal of the server.
    console.log("Nodejs tcp server running at port 4001\n");
}

function sendMessageToServer(msg){

    var jsonData = JSON.parse(msg);
    var requestId = jsonData.RequestID;
    var method  = jsonData.Command=='get'?'GET':'POST';
    //console.log('method - > ' + method);
    // An object of options to indicate where to post to
    var post_options = {
        host: SERVER_HOST,
        port: SERVER_PORT,
        path: '',
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': msg.length
        }
    };

    try {
        // Set up the request
        var post_req = http.request(post_options, function (res) {

            res.setEncoding('utf8');
            var output = '';
            res.on('data', function (data) {
                output += data;
            });

            res.on('end', function () {
                try {
                    //in login - get from the response the api_key and save it for the session id
                    if (output !== null && output !== undefined) {
                        var obj = JSON.parse(output);

                        var response = responses[obj.RequestID];
                        //checks if the object contains API_KEY
                        if (obj.API_KEY !== null && obj.API_KEY !== undefined) {
                            var unixtime = Math.round((new Date()).getTime() / 1000);
                            //if the action is validate and the entity is login
                            //take the user id and save it in a JSON format: {UserID:1234,UnixTIme:123322333}
                            if (obj.Command == 'validate' && obj.Entity == 'login') {

                                if (obj.Result == "Error") {
                                    //var response = responses[obj.RequestID];
                                    if (response != undefined) {
                                        response.statusCode = 401;
                                        obj.Reason = "Wrong username/password";
                                        output= JSON.stringify(obj);
                                    }
                                }
                                else {
                                    //checks if the user already exists in the users object
                                    if (users[obj.UserID] !== null && users[obj.UserID] !== undefined) {
                                        //console.log('user exists - ' + obj.UserID + ' getting the request ... ');
                                        //get the request from the existing list
                                        var request = requests[obj.RequestID];
                                        //if found something
                                        if (request !== null && request !== undefined) {
                                            //console.log('found request, checks if ip is the same : ' + request.connection.remoteAddress + ' <-> ' + users[obj.UserID]);
                                            //checks if it is from the same IP
                                            if (request.connection.remoteAddress !== users[obj.UserID]) {
                                                //console.log('deleting the old request object');
                                            }
                                            //save the new one
                                            users[obj.UserID] = request.connection.remoteAddress;
                                        }
                                    }
                                    //console.log('save userid- > ' + obj.UserID);
                                    var userid = obj.UserID;
                                    clients[obj.API_KEY] = JSON.stringify({
                                        'UserID': userid,
                                        'unixtime': unixtime,
                                        'UserName': obj.UserName,
                                        'AllowedDevices': obj.AllowedDevices
                                    });
                                    //var response = responses[obj.RequestID];
                                    if (response != undefined) {
                                        response.statusCode = 200;

                                        var max_age = 24*60*60*1000; // after 24 hours

                                        response.cookie('api_key', obj.API_KEY, {
                                            maxAge: max_age,
                                            httpOnly: true
                                        });
                                        response.cookie('user', JSON.stringify({
                                            'username': obj.UserName,
                                            'role': obj.RoleID
                                        }), {maxAge: max_age, httpOnly: false});
                                    }
                                }
                            }
                            else {
                                //console.log('update unixtime');
                                var json = JSON.parse(clients[obj.API_KEY]);
                                json.unixtime = unixtime;
                                //console.log('userid updated -> ' + json.UserID);
                                clients[obj.API_KEY] = JSON.stringify(json);

                                if (response != undefined) {
                                    response.statusCode = 200;
                                    //if contains error then return status code 500
                                    if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                                        response.statusCode = 400;
                                    }
                                    else if (obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                                        //response.statusCode = 422;
                                    }
                                    else if(obj.Result.toUpperCase().indexOf('PERMISSION_DENIED')>=0){
                                        response.statusCode = 550;
                                    }
                                    else if(obj.Result.toUpperCase().indexOf('SESSION_TIMEOUT')>=0){
                                        response.statusCode = 401;
                                    }
                                }
                            }
                        }
                        //delete the request
                        delete responses[obj.RequestID];
                  //checks the response from the server if was ok
                        if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                            response.statusCode = 400;
                        }
                        else if (obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                            //response.statusCode = 422;
                        }
                        else if(obj.Result.toUpperCase().indexOf('PERMISSION_DENIED')>=0){
                            response.statusCode = 550;
                        }
                        else if(obj.Result.toUpperCase().indexOf('SESSION_TIMEOUT')>=0){
                            response.statusCode = 401;
                        }
                        //send back the response
                        response.json(JSON.parse(output));
                    }
                    //}
                }
                catch (e) {
                    console.log('Fail in parsing message from server... ');
                    console.log(e);
                    var responseError = responses[requestId];
                    if (responseError != undefined) {
                        responseError.statusCode = 400;
                        delete responses[requestId];
                        responseError.json(output);
                    }
                }

            });
        });

        // post the request
        post_req.write(msg);

        post_req.on('error', function (err) {
            console.log(err);
            var responseError = responses[requestId];
            if (responseError != undefined) {
                responseError.statusCode = 400;
                delete responses[requestId];
                responseError.json(msg);
            }
        });

        post_req.end();

    }
    catch(ex) {
        console.log('Fail while trying to communicate with the Server API');
        console.log(ex);
    }

}

function validateSession(requestid,req,res){

    var unixtime = Math.round((new Date()).getTime() / 1000);
    var apikey= req.cookies.api_key;

    if(clients[apikey] !== null && clients[apikey] !== undefined)
    {
        var json =  (JSON.parse(clients[apikey]));
        //console.log('after parse the clients object');
        //console.log(json);
        var savedUnixtime = json.unixtime;

        //console.log('1-> ' + clients[apikey]);
        //console.log('unixtime - > ' + unixtime);

        if((unixtime-clients[apikey])>API_KEY_LIMIT_TIME)
        {
            //console.log('session expired...');
            delete clients[apikey];
            res.statusCode =401;
            res.statusText="session expired! Please re-login";
            res.send({data:{Error:'session expired! Please re-login'}});
        }
        else
        {
            //console.log('session is ok :-) ');
            //renew the session time with the current time
            //clients[req.cookies.api_key] = unixtime;
            var json = JSON.parse(clients[apikey]) ;
            json.unixtime =  unixtime;
            clients[apikey]  = JSON.stringify(json);
            //save this request id
            responses[requestid] = res;
            requests[requestid] = req;//req.connection.remoteAddress
            users[json.UserID]=req.connection.remoteAddress;
            res.statusCode =200;
            res.statusText="";
            return 1;
        }
    }
    else
    {
        //console.log('no api key found');
        res.statusCode =401;
        res.statusText="session expired! Please re-login";
        res.send({data:{Error:'session expired! Please re-login'}});
    }

    return 0;
}

function getParams (req,res){
    var requestid = req.id;
    var method = extractMethod(req);
    var command = 'get';

    var data ='';
    if(req.query!== null && req.query !== undefined){
        data = req.query;
    }

    if (typeof data.Action != 'undefined')
        command = data.Action.toLowerCase();

    data = JSON.stringify(req.query);
    //validate the session id (api key)
    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,command,method,data,0));

}

function buildMsg(apikey,requestid,command,entity,data,siteId){
    //change the format to {RequestID:,UserID:,Command:,Entity:,Data:}
    //on each action, send the user id to validate if is allowed to do this action
    //get the user id from the clients object
    //console.log('api key -> ' + apikey);
    var userid = 0;
    if(apikey.length>0 && clients[apikey]!=='null' && clients[apikey] !=='undefined')
    {
        //console.log('try to get the json object');
        var objJson = JSON.parse(clients[apikey]);
        //console.log(objJson);
        //console.log('the userid found -> ' + objJson.UserID);
        userid = objJson.UserID;
    }

    var json ='{"RequestID":"'  + requestid + '","UserID":"' + userid + '","SiteID":"' + siteId + '","Command":"' + command + '","Entity":"' + entity + '","Data":' + data + '}';
    //var json =JSON.stringify({
    //    RequestID:  requestid,
    //    UserID : userid,
    //    SiteID: siteId,
    //    Command: command,
    //    Entity: entity,
    //    Data: "'" + data + "'"
    //});

    return (json);
}

function extractMethod(req){
    var method = req.url.slice(req.url.indexOf('/api/') + 5);
    //console.log(method);
    if(method.indexOf('/')>0){
        method = method.slice(0,method.indexOf('/'));
    }
    else if (method.indexOf('?')>0){
        method=method.slice(0,method.indexOf('?'));
    }
    else
        method = method.slice(0);
    return method.toLowerCase();
}

function calibrationActions  (req,res){

    var requestid = req.id;

    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;
        console.log('calibrate method -> ' + method);
        //validate the session id (api key)
        if (validateSession(requestid, req, res)){
            var  siteId=0;
            if(req.body.SiteID!== undefined && req.body.SiteID!== null)
                 siteId = req.body.SiteID;
            sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'calibration', JSON.stringify(req.body),siteId));
        }
    }
}

function Restore (req,res){
    var requestid = req.id;
    if(validateSession(requestid,req,res))
    {
        var json = getJsonParams(req);
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'restore','dbmaintain',json,0));
    }
}

function device (req,res){
    var requestid = req.id;
    var method = extractMethod(req);
    var json = getJsonParams(req);

    //validate the session id (api key)
    if(validateSession(requestid,req,res))
    {
        //call to core to get the response
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'device',json,0));
    }

}

function deviceActions (req,res){
    var requestid = req.id;

    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

        if(method!==undefined){
            console.log('method -> ' + method);
            //validate the session id (api key)
            if(validateSession(requestid,req,res)){
                //call to core to get the response
                var siteId = 0;
                if(req.body.SiteID)
                    siteId=req.body.SiteID;
                sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'device',JSON.stringify(req.body),siteId));
            }
        }
    }
}

function sitesActions (req,res){
    var requestid = req.id;

    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

        if(method!==undefined)
        {
            console.log('method -> ' + method);
            //validate the session id (api key)
            if(validateSession(requestid,req,res))
            {
                var json = req.body;
                if(req.originalMethod == "DELETE")
                    json = {
                        "ID": req.params.id
                    };
                //call to core to get the response
                sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'sites',JSON.stringify(json),0));
            }
        }
    }
}

function alarmActions (req,res){
    var requestid = req.id;

    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if(validateSession(requestid,req,res))
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'alarms',JSON.stringify(req.body),0));
    }
}

function reportUploadHeader (req,res){

    var requestId = req.id;

    responses[requestId] = res;
    requests[requestId] = req;

    var file = req.file;

    var post_options = {
        host: SERVER_HOST,
        port: SERVER_PORT,
        path: '/upload/?requestid=' + requestId,
        method: 'POST',
        headers: {
            'Content-Type': file.mimetype,
            'Content-Length': file.size
        }

    };
    var post_req = http.request(post_options, function (res) {
        res.setEncoding('utf8');

        var output = '';
        res.on('data', function (data) {
            output += data;
        });

        res.on('end', function () {
            try {

                if (output !== null && output !== undefined) {
                    //var jsonMsg = output.split('#')[1];
                    var obj = JSON.parse(output);

                    var response = responses[obj.RequestID];
                    if (response != undefined) {
                        response.statusCode = 200;
                        //if contains error then return status code 500
                        if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                            console.log('message returns from server contains ERROR');
                            response.statusCode = 500;
                        }
                        else if (obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                            //response.statusCode = 422;
                        }
                        //delete the request
                        delete responses[obj.RequestID];
                        //send back the response
                        response.json(JSON.parse(output));
                    }
                }
            }
            catch (e) {
                console.log('Fail in parsing message from server... ');
                console.log(e);
                var responseError = responses[requestId];
                if (responseError != undefined) {
                    responseError.statusCode = 500;
                    delete responses[requestId];
                    responseError.json(output);
                }
            }
        });

    });

    var img = fs.readFileSync(file.path);
    // post the request
    post_req.write(img,'binary');

    post_req.on('error', function (err) {
        console.log(err);
        var responseError = responses[requestId];
        if (responseError != undefined) {
            responseError.statusCode = 500;
            delete responses[requestId];
            responseError.json(msg);
        }
    });

    post_req.end();
}

function reportsActions (req,res){

    var requestid = req.id;

    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

        console.log('method -> ' + method);

        //validate the session id (api key)
        if(validateSession(requestid,req,res))
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'reports',JSON.stringify(req.body),0));
    }

}

function analyticsActions(req,res){
    var requestid = req.id;

    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if(validateSession(requestid,req,res))
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,method.toString().toLowerCase(),'analytics',JSON.stringify(req.body),0));
    }
}

function systemUploadCSV (req,res){

    var requestId = req.id;
    var userId=req.params.userId;
    responses[requestId] = res;
    requests[requestId] = req;

    var file = req.file;

    var post_options = {
        host: SERVER_HOST,
        port: SERVER_PORT,
        path: '/upload/?requestid=' + requestId + '&userid=' + userId,
        method: 'POST',
        headers: {
            'Content-Type': file.mimetype,
            'Content-Length': file.size
        }

    };
    var post_req = http.request(post_options, function (res) {
        res.setEncoding('utf8');

        var output = '';
        res.on('data', function (data) {
            output += data;
        });

        res.on('end', function () {
            try {

                if (output !== null && output !== undefined) {
                    //var jsonMsg = output.split('#')[1];
                    var obj = JSON.parse(output);

                    var response = responses[obj.RequestID];
                    if (response != undefined) {
                        response.statusCode = 200;
                        //if contains error then return status code 500
                        if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                            console.log('message returns from server contains ERROR');
                            response.statusCode = 500;
                        }
                        else if (obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                           // response.statusCode = 422;
                        }
                        //delete the request
                        delete responses[obj.RequestID];
                        //send back the response
                        response.json(JSON.parse(output));
                    }
                }
            }
            catch (e) {
                console.log('Fail in parsing message from server... ');
                console.log(e);
                var responseError = responses[requestId];
                if (responseError != undefined) {
                    responseError.statusCode = 500;
                    delete responses[requestId];
                    responseError.json(output);
                }
            }
        });

    });

    var csv = fs.readFileSync(file.path);
    // post the request
    post_req.write(csv,'binary');

    post_req.on('error', function (err) {
        console.log(err);
        var responseError = responses[requestId];
        if (responseError != undefined) {
            responseError.statusCode = 500;
            delete responses[requestId];
            responseError.json("");
        }
    });

    post_req.end();
}

function generalActions(req,res){
    var requestid = req.id;
    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined) {
        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if (validateSession(requestid, req, res))
            sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'general', JSON.stringify(req.body),0));
    }
}

function databaseActions(req,res){
    var requestid = req.id;
    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined) {

        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if (validateSession(requestid, req, res)) {

            if (method === 'backup') {

                if (clients[req.cookies.api_key] !== undefined) {

                    var userID = JSON.parse(clients[req.cookies.api_key]).UserID;

                    var options = {
                        // host: urlObject.parse(method).host,
                        hostname: SERVER_HOST,
                        port: SERVER_PORT,
                        path: '/?userid=' + userID,
                        method: method,
                        headers: {
                            'Content-Type': 'application/bak',
                            'Content-Length': 255
                        }
                    };

                    var milliseconds = (new Date).getTime();

                    var file_name = 'backup-' + milliseconds + '.bson';
                    var file = fs.createWriteStream( DOWNLOAD_DIR + file_name);

                    http.get(options, function (response) {
                        response.on('data', function (data) {
                            file.write(data);
                        }).on('end', function () {
                            file.end();

                            res.status(200).json({ fileName: file_name})
                        });
                    });
                }
            }
            else if (method === 'restore') {
                uploadDatabaseFile(req,res);
            }
        }
    }
}

function uploadDatabaseFile(req,res){
    var requestId = req.id;

    responses[requestId] = res;
    requests[requestId] = req;

    var file = req.file;

    var post_options = {
        host: SERVER_HOST,
        port: SERVER_PORT,
        path: '/upload/?requestid=' + requestId,
        method: 'POST',
        headers: {
            'Content-Type': file.mimetype,
            'Content-Length': file.size
        }

    };
    var post_req = http.request(post_options, function (res) {
        res.setEncoding('utf8');

        var output = '';
        res.on('data', function (data) {
            output += data;
        });

        res.on('end', function () {
            try {

                if (output !== null && output !== undefined) {
                    //var jsonMsg = output.split('#')[1];
                    var obj = JSON.parse(output);

                    var response = responses[obj.RequestID];
                    if (response != undefined) {
                        response.statusCode = 200;
                        //if contains error then return status code 500
                        if (obj.Result.toUpperCase().indexOf('ERROR') >= 0) {
                            console.log('message returns from server contains ERROR');
                            response.statusCode = 500;
                        }
                        else if (obj.Result.toUpperCase().indexOf('NO_DATA') >= 0) {
                           // response.statusCode = 422;
                        }
                        //delete the request
                        delete responses[obj.RequestID];
                        //send back the response
                        response.json(JSON.parse(output));
                    }
                }
            }
            catch (e) {
                console.log('Fail in parsing message from server... ');
                console.log(e);
                var responseError = responses[requestId];
                if (responseError != undefined) {
                    responseError.statusCode = 500;
                    delete responses[requestId];
                    responseError.json(output);
                }
            }
        });

    });

    var img = fs.readFileSync(file.path);
    // post the request
    post_req.write(img,'binary');

    post_req.on('error', function (err) {
        console.log(err);
        var responseError = responses[requestId];
        if (responseError != undefined) {
            responseError.statusCode = 500;
            delete responses[requestId];
            responseError.json(msg);
        }
    });

    post_req.end();
}

function contactsActions(req,res){
    var requestid = req.id;
    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined) {
        var method = req.query.Action;
        console.log('method -> ' + method);
        //validate the session id (api key)
        if (validateSession(requestid, req, res))
            sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'contacts', JSON.stringify(req.body),0));
    }
}

function userActions (req,res){
    var requestid = req.id;
    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined && req.query.Action !== undefined) {
        var method = req.query.Action.toString().toLowerCase();
        console.log('method -> ' + method);
        if(method=== 'uploadTreeStructure'){
            systemUploadCSV(req,res);
        }
        else {
            //validate the session id (api key)
            if (validateSession(requestid, req, res))
                if(method==='saveuser' && req.url.indexOf('account-profile')>0)
                    method = 'saveaccountprofile';
                sendMessageToServer(buildMsg(req.cookies.api_key, requestid, method.toString().toLowerCase(), 'user', JSON.stringify(req.body),0));
        }
    }
    else{
        responses[requestid] = res;
        requests[requestid] = req;

        sendMessageToServer(buildMsg('', requestid, 'passwordrecovery', 'user', JSON.stringify(req.body),0));
    }
}

function signupActions(req,res){

    var requestid = req.id;

    responses[requestid] = res;
    requests[requestid] = req;


    //var method = extractMethod(req);
    if(req.query!== null && req.query !== undefined) {
        var method = req.query.Action;
        console.log('method -> ' + method);
        var entity = 'signup';
        if(method==='registerCustomer' || method ==='Signup' || method==='getCustomers')
            entity='customers';
        if(req.body.state)
            if(req.body.state==='register_customer')
                entity='customers';
        var json = buildMsg('',requestid,method.toString().toLowerCase(),entity,JSON.stringify(req.body),0);
        sendMessageToServer(json);
    }
}

function getFileCsv (req,res){
    if(req.query!== null && req.query !== undefined){

        var method = req.query.Action;

        if (!Date.now) {
            Date.now = function() { return new Date().getTime(); }
        }

        var fileTimeStamp = Math.floor(Date.now() / 1000);

        var objJson = JSON.parse(clients[req.cookies.api_key]);
        var userid = objJson.UserID;
        var entity = urlObject.parse(req.url).pathname.split('/')[2].toLowerCase();
        var source = entity.concat('-',urlObject.parse(req.url).pathname.split('/')[3].toLowerCase());
        var contentType = method==='getCSV'?'text/csv':'application/pdf';
        var options = {
            host: SERVER_HOST,
            port: SERVER_PORT,
            path: req.url + '&userid=' + userid + '&source=' + source, //urlObject.parse(req.url).pathname,
            headers: {
                'Content-Type': contentType
            }
        };

        var extension = method==='getCSV'?'.csv':'.pdf';
        var csvSource = urlObject.parse(req.url).pathname.split('/').pop().concat('-',fileTimeStamp,extension);
        var file = fs.createWriteStream(DOWNLOAD_DIR + csvSource);

        var request = http.get(options, function(response) {
            response.on('data', function(data) {
                file.write(data);
            }).on('end', function() {
                file.end();

                var file2Download = 'SiMiO_' + source + extension;
                res.download(DOWNLOAD_DIR + csvSource,file2Download );
            });
        });
        request.on('error', function(err) {
            //console.log(err);
        });

        request.end();
    }

}

function getFile (req,res){
    if(req.query!== null && req.query !== undefined){
        var action = req.query.Action;

        //res.writeHead(200,{
        //    "Content-Disposition": "attachment; filename='" + urlObject.parse(action).pathname.split('/').pop() + "'",
        //    "Content-Type": "application/pdf"
        //});

        var options = {
            host: urlObject.parse(action).host,
            port: 80,
            path: urlObject.parse(action).pathname
        };

        var file_name = urlObject.parse(action).pathname.split('/').pop();
        var file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

        http.get(options, function(response) {
            response.on('data', function(data) {
                file.write(data);
            }).on('end', function() {
                file.end();
                res.download(DOWNLOAD_DIR + file_name, 'certificate.pdf');
            });
        });

    }

}

function get (req,res){
    var requestid = req.id;
    var method = extractMethod(req);

    //validate the session id (api key)
    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'get', method,'',0));
}

function del (req,res){
    var requestid = req.id;
    //extract the method from the api call
    var method = extractMethod(req);
    var json = getJsonParams(req);

    //validate the session id (api key)
    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'delete', method,json,0));
}

function update (req,res) {

    var requestid = req.id;
    var method = extractMethod(req);

    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'update', method,JSON.stringify(req.body),0));
}

function setup (req,res) {

    var requestid = req.id;
    var method = extractMethod(req);

    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'setup', method,JSON.stringify(req.body),0));
}

function create (req,res) {

    var requestid = req.id;
    var method = extractMethod(req);

    if(validateSession(requestid,req,res))
        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'create', method,JSON.stringify(req.body),0));
}

function SMSDelivery (req,res) {

    sendMessageToServer(buildMsg('',req.id,'smsdelivery', 'sms',JSON.stringify(req.body),0));

}

function login (req, res) {

    var requestid = req.id;
    //save the response object with the request-id key
    responses[requestid] = res;
    requests[requestid] = req;
    //var json = buildMsg('',requestid,'validate','login',JSON.stringify(req.body),0);

    var isCustomerExists = 0;
    if(req.body.Customer!==undefined)
        isCustomerExists=1;
    var body = JSON.stringify({
        'UserName': req.body.UserName,
        'Password': req.body.Password,
        'Customer': req.body.Customer,
        'CustomerExists' : isCustomerExists
    });
    var json = buildMsg('',requestid,'validate','login',body,0);
    sendMessageToServer(json);
}

function logout (req, res) {
    res.statusCode = 200;

    if (req.cookies.api_key) {
        delete clients[req.cookies.api_key];
        res.clearCookie('api_key');
        res.json({result:{data:'logged out'}});
        var socket = socketClients[req.cookies.api_key];
        if(socket)
            socket.disconnect();
    }
}

function sessionCheck(req,res){
    var requestid = req.id;

    if(validateSession(requestid,req,res)==0)
    {
        res.statusCode =401;
        res.send('session expired! Please re-login');
    }
    else
    {
        res.statusCode =200;

        if(clients[req.cookies.api_key]!== undefined && clients[req.cookies.api_key].UserID!== undefined)
        {
            //get the user details according to user id that extract from the api_key
            var json = '{UserID:' +  clients[req.cookies.api_key].UserID + '}';
            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'get','Login',json,0));
        }
        else
        {
            res.statusCode =401;
            res.send('session expired! Please re-login');
        }
    }
}

app.get('/api/Settings/database/:name', function (req, res, next) {

    var file_name = req.params.name;
    var fullPath = DOWNLOAD_DIR + file_name;
    var stats = fs.statSync(fullPath);
    var fileSize = stats["size"];
    var options = {
        root: DOWNLOAD_DIR,
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true,
            'Content-Disposition': "attachment; filename=" + file_name,
            'Content-Type': "application/bson",
            'Content-Length': fileSize
        }
    };
    res.sendFile(file_name, options, function (err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        }
        else {
            console.log('Sent:', file_name);
        }
    });

});

app.get('/api/Sites', getParams);
app.get('/api/LanRcvNetworkTree', getParams);
app.get('/api/LastSampleAllDevices', getParams);
app.get('/api/AlarmOnSites', getParams);
app.get('/api/GPSDevices', getParams);
app.get('/api/DeviceHistoryLogs', getParams);
app.get('/api/DeviceGraphData', getParams);
app.get('/api/DeviceSetupTemplate', getParams);
app.get('/api/DevicesLastSample', getParams);
app.get('/api/DeviceAlarms', getParams);
app.get('/api/CustomSensors', getParams);
app.get('/api/MeasurementUnits', getParams);
app.get('/api/AlarmTypes', getParams);
app.get('/api/ReportPeriodTypes', getParams);
app.get('/api/ReportFormatTypes', getParams);
app.get('/api/SensorTypes', getParams);
app.get('/api/Devices', getParams);
app.get('/api/GroupPermissions', getParams);
app.get('/api/SecuredQuestions', getParams);
app.get('/api/Categories', getParams);
app.get('/api/Languages', getParams);
app.get('/api/Users', getParams);
app.get('/api/Contacts', getParams);
app.get('/api/Alarms',getParams);
app.get('/api/Analytics', getParams);
app.get('/api/Reports',getParams);
app.get('/api/Reports/reviews',getParams);
app.get('/api/Reports/profiles',getParams);
app.get('/api/Status',device);
app.get('/api/Sensors',device);
app.get('/api/DownloadData',device);
app.get('/api/Devices/:id', deviceActions);
app.get('/api/Me',sessionCheck);
app.get('/api/Logout', logout);
app.get('/api/Settings/user-management', userActions);
app.get('/api/CertificateFile', getFile);
app.get('/api/Settings/account-profile/:userId',userActions);
app.get('/api/Settings/system-settings',userActions);
app.get('/api/Settings/custom-sensors',deviceActions);
app.get('/api/Settings/notifications',alarmActions);
app.get('/api/Settings/user-management',userActions);
app.get('/api/Settings/account-profile',userActions);
app.get('/api/Settings/contact-lists',contactsActions);
app.get('/api/Settings/database',databaseActions);
app.get('/api/General',generalActions);
app.get('/api/Analytics/table',getFileCsv);
app.get('/api/Analytics/statistics',analyticsActions);
app.get('/api/Analytics/mkt',analyticsActions);
app.get('/api/Analytics/histogram',getFileCsv);
app.get('/api/Analytics/dewpoint',getFileCsv);
app.get('/api/Signup',signupActions);

app.post('/api/Login', login);
app.post('/api/Customers', create);
app.post('/api/Admin', create);
app.post('/api/Users', create);
app.post('/api/CustomSensor', create);
app.post('/api/alarmSetup', create);
app.post('/api/lockNetwork', create);
app.post('/api/setupLanRcvCredentials', create);
app.post('/api/SensorCalibration/:id', calibrationActions);
app.post('/api/markDeviceTimeStamp', create);
app.post('/api/ProfileReport', create);
app.post('/api/setupSMS', create);
app.post('/api/Contacts', create);
app.post('/api/DeviceSetupTemplate', create);
app.post('/api/SMS',SMSDelivery);
app.post('/api/Devices/:id', deviceActions);
app.post('/api/Devices', deviceActions);
app.post('/api/Alarms/:id', alarmActions);
app.post('/api/Sites/:id', sitesActions);
app.post('/api/Sites', sitesActions);
app.post('/api/Alarms', alarmActions);
app.post('/api/Analytics/:state', analyticsActions);
app.post('/api/Analytics', analyticsActions);
app.post('/api/Reports/archive',reportsActions);
app.post('/api/Reports',reportsActions);
app.post('/api/Reports/reviews',reportsActions);
app.post('/api/Reports/profiles',reportsActions);
app.post('/api/Reports/new-profile/fileUpload',reportUploadHeader);
app.post('/api/Settings/account-profile/:userId',userActions);
app.post('/api/Settings/system-settings',userActions);
app.post('/api/Settings/system-settings/:userId',userActions);
app.post('/api/Settings/custom-sensors',deviceActions);
app.post('/api/Settings/notifications',alarmActions);
app.post('/api/Settings/user-management',userActions);
app.post('/api/Settings/audit-trail',userActions);
app.post('/api/Settings/contact-lists',contactsActions);
app.post('/apiSettings/database',databaseActions);
app.post('/api/Signup',signupActions);
app.post('/api/recovery',userActions);

app.delete('/api/Users/:UserID', del);
app.delete('/api/ProfileReport/:id', del);
app.delete('/api/Contacts/:id', del);
app.delete('/api/Sites/:id', sitesActions);

//api.loadSocket(server);
server.listen(4433);



//function setSession (req,res){
//    var requestid = req.id;
//
//    if(validateSession(requestid,req,res)==0)
//    {
//        res.statusCode =401;
//        res.send('session expired! Please re-login');
//    }
//    else
//    {
//        res.statusCode =200;
//
//        if(clients[req.cookies.api_key]!== undefined && clients[req.cookies.api_key].UserID!== undefined){
//            //get the user details according to user id that extract from the api_key
//            var json = '{UserID:' +  clients[req.cookies.api_key].UserID + '}';
//            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'get','Login',json,0));
//        }
//        else{
//            res.statusCode =401;
//            res.send('session expired! Please re-login');
//        }
//    }
//}

//function Backup (req,res){
//    var requestid = req.id;
//    if(validateSession(requestid,req,res))
//    {
//        var json = getJsonParams(req);
//        //logger.debug('params - ' + json);
//
//        sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'backup','dbmaintain',json,0));
//    }
//}

//function getQuery (req,res){
//    var requestid = req.id;
//
//    var method = extractMethod(req);
//
//
//    if(validateSession(requestid,req,res))
//    {
//        //call to core to get the response
//        if(req.query.CategoryID !==null && req.query.CategoryID!== undefined)
//        {
//            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'category','devices','{CategoryID:' + req.query.CategoryID + '}'));
//        }
//        else if(req.query.SiteID !==null && req.query.SiteID!== undefined
//            &&
//            req.query.UserID !==null && req.query.UserID!== undefined )
//        {
//            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'site','devices','{SiteID:' + req.query.SiteID + ',UserID:' + req.query.UserID + '}'));
//        }
//        else if(req.query.DeviceID !==null && req.query.DeviceID!== undefined)
//        {
//            //this is for single device
//            sendMessageToServer(buildMsg(req.cookies.api_key,requestid,'device','devices','{DeviceID:' + req.query.DeviceID + '}'));
//        }
//
//    }
//}

//function passwordRecovery (req,res){
//    var requestid = req.id;
//
//    var method = extractMethod(req);
//
//    responses[requestid] = res;
//    requests[requestid] = req;
//
//    //call to core to get the response
//    sendMessageToServer(buildMsg("",requestid,method.toString().toLowerCase(),'user',JSON.stringify(req.body),0));
//
//}
